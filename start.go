package main

import (
	"log"
	"net/http"
)

func main() {
	http.HandleFunc("/hola", func(w http.ResponseWriter, r *http.Request) {
		w.Write([]byte("hola desde aws"))
	})
	log.Fatal(http.ListenAndServe(":2018", nil))
}
